filetype on		      " for osx need to turn on first
set nocompatible    " be iMproved, required
filetype off        " required for vundle - we will turn it back on later


""""""""""""""""""""""""""""
"Vundle Plugin Configuration
""""""""""""""""""""""""""""

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim/
let path='~/.vim/bundle'
call vundle#begin(path)
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

"Notetaking plugins/settings
Bundle 'jtratner/vim-flavored-markdown'
augroup markdown
    au!
    au BufNewFile,BufRead,BufWrite *.md,*.markdown setlocal filetype=ghmarkdown
    au BufNewFile,BufRead *.md,*.markdown setlocal nonumber
augroup END

"Syntastic - Lint all sorts of languages
Bundle 'scrooloose/syntastic'
let g:syntastic_php_checkers = ['php']

"PHP
""Vdebug - Requires python with vim
"Bundle 'joonty/vdebug.git'
"let g:vdebug_options = {
"	    \ 'path_maps': {"/srv/www/wordpress-default": "/Users/kraske/vvv/www/wordpress-default"},
"	    \}

"Javascript
""Enhanced JavaScript Syntax
Bundle 'jelera/vim-javascript-syntax'
"Enhanced JavaScript IDE tools (TERN for vim) - Requires Extra Configuration
" - Use npm to install tern
" - Navigate to plugin installation directory and do npm install
" - Ensure python is installed and 'python' command is in PATH
"Bundle 'marijnh/tern_for_vim'
""Proper Javascript Indentation
Bundle 'vim-scripts/JavaScript-Indent'

"HTML
""Emmet.vim - Zen Coding
Bundle 'mattn/emmet-vim'

"UI Enhancement Plugins
""Btter file tree
Bundle 'scrooloose/nerdtree'
""Better ctag navigator
Bundle 'majutsushi/tagbar'
""Fuzzy file finder
Bundle 'kien/ctrlp.vim'
""Integrated Git tools
Bundle 'tpope/vim-fugitive'
""Fancy Autocomplete. Has many code dependencies. For OSX this means
""Getting XCode installed on your system
"Bundle 'https://github.com/Valloric/YouCompleteMe'

"Productivity Tools
""Fancy comments
Bundle 'scrooloose/nerdcommenter'
""Solving minor case annoyences
Bundle 'tpope/vim-abolish'
""Surround.vim - good for surrounding text with tags or quotes
Bundle 'tpope/vim-surround'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList          - list configured plugins
" :PluginInstall(!)    - install (update) plugins
" :PluginSearch(!) foo - search (or refresh cache first) for foo
" :PluginClean(!)      - confirm (or auto-approve) removal of unused plugins
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line


"""""""""""""""""""""
"My Preferences
"""""""""""""""""""""

"Editor Behaviors
filetype plugin indent on
syntax enable
set autoindent                       "keep indentation on next line
set ignorecase                       "ignore case with / or ? searching
set smartcase                        "ignore case in search if lowercase, but use if uppercase
set smarttab                         "tabs for indents, spaces for alignment
set noexpandtab                      "use hard tabs (not expanded to spaces)
set tabstop=2												 "tab length is 2 characters
set shiftwidth=2                     "tab length is 2 characters
set softtabstop=2                    "tab length is 2 characters
set backspace=indent,eol,start       "allow backspace between lines in insert mode
set omnifunc=syntaxcomplete#Complete "use omnicomplete for syntax
set completeopt=longest,menuone			 "make omnicomplete work like a sane ide
set hlsearch                         "highlight things with / or ? search
set autoread                         "autodetect external changes to file - may not work
set number                           "line numbers
set colorcolumn=80                   "column suggestion lines
set splitright                       "split new windows right instead of default left
set splitbelow                       "split new windows down instead of default above
set ruler                            "show column info at bottom (at the moment overwritten by "statusline" lines)
set laststatus=2                     "always display the status line

" Show a fancy status line
" From a Stackoverflow Answer http://stackoverflow.com/questions/5375240/a-more-useful-statusline-in-vim
set statusline=
set statusline +=%1*\ %n\ %*            "buffer number
set statusline +=%5*%{&ff}%*            "file format
set statusline +=%3*%y%*                "file type
set statusline +=%4*\ %<%F%*            "full path
set statusline +=%2*%m%*                "modified flag
set statusline +=%1*%=%5l%*             "current line
set statusline +=%2*/%L%*               "total lines
set statusline +=%1*%4v\ %*             "virtual column number
set statusline +=%2*0x%04B\ %*          "character under cursor

" System Behaviors
set clipboard=unnamed "Use system clipboard by default
set tags=./tags;      "Use recursive search for tags file from working dir

"Keymappings
inoremap <C-space> <C-x><C-o>
nnoremap <silent> <C-Right> <c-w>l
nnoremap <silent> <C-Left> <c-w>h
nnoremap <silent> <C-Up> <c-w>k
nnoremap <silent> <C-Down> <c-w>j

"Make it pretty
color distinguished
set guifont=Monaco:h12
