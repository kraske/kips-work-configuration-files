# README #

This repository is just a collection of configuration files that I have on my OSX machine at work that have collected some useful tools for me.

* .vimrc - My Vim plugins and configuration settings. Note I use [vundle](https://github.com/gmarik/Vundle.vim) so you will have to install that to use the plugins that I have here.
* .bash_profile - my bash profile. I have some useful aliases and git settings here
* .gitconfig and .gitignore_global are my local machine git settings