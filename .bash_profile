# Folder colors (why is this not default on osx?)
export CLICOLOR=1

#Use MacVim for our git editor
export GIT_EDITOR='/Applications/MacVim.app/Contents/MacOS/Vim -f '

#Extra things installed to home directory that I would like in path
export PATH=$PATH:~/PHP_CodeSniffer/bin

#Aliases
alias py='python3.7'

# Project CD Paths for quick access
export CDPATH=.:~:~/vvv/www
export CDPATH=$CDPATH:~/vvv/www/wordpress-default/public_html/wp-content/themes/:~/vvv/www/wordpress-default/public_html/wp-content/plugins/
export CDPATH=$CDPATH:~/vvv/www/chauvenet/public_html/wp-content/themes/:~/vvv/www/chauvenet/public_html/wp-content/plugins/
export CDPATH=$CDPATH:~/vvv/www/research/public_html/wp-content/themes/:~/vvv/www/research/public_html/wp-content/plugins/
export CDPATH=$CDPATH:~/vvv/www/diversity/public_html/wp-content/themes/:~/vvv/www/diversity/public_html/wp-content/plugins/
export CDPATH=$CDPATH:~/vvv/www/students/public_html/wp-content/themes/:~/vvv/www/students/public_html/wp-content/plugins/
export CDPATH=$CDPATH:~/vvv/www/source/public_html/wp-content/themes/:~/vvv/www/source/public_html/wp-content/plugins/
export CDPATH=$CDPATH:~/vvv/www/admissions-aid/public_html/wp-content/themes/:~/vvv/www/admissions-aid/public_html/wp-content/plugins/

# Git things
alias git-cleanbranches='git branch --merged | grep -v "\*" | grep -v master | grep -v dev | xargs -n 1 git branch -d'

# Show git status and custom prompt
# parse_git_dirty () {
#     [[ $(git status 2> /dev/null | tail -n1) != "nothing to commit, working directory clean" ]] && echo "*"
# }
parse_git_dirty () {
    echo "";
}
parse_git_branch () {
    git branch --no-color 2> /dev/null | sed -e '/^[^*]/d' -e "s/* \(.*\)/\(\1$(parse_git_dirty)\)/"
}

#Note, the real host you can get with \h, but I just put pa
PS1="\u@pa:\w\$([[ -n \$(git branch 2> /dev/null) ]] && echo)\$(parse_git_branch)"

# Git autocomplete
if [ -f `brew --prefix`/etc/bash_completion ]; then
        . `brew --prefix`/etc/bash_completion
fi

# SSH set up
SSH_ENV=$HOME/.ssh/environment

# start the ssh-agent
function start_agent {
    echo "Initializing new SSH agent..."
    # spawn ssh-agent
    /usr/bin/ssh-agent | sed 's/^echo/#echo/' > "${SSH_ENV}"
    echo succeeded
    chmod 600 "${SSH_ENV}"
    . "${SSH_ENV}" > /dev/null
    /usr/bin/ssh-add
}

if [ -f "${SSH_ENV}" ]; then
     . "${SSH_ENV}" > /dev/null
     ps -ef | grep ${SSH_AGENT_PID} | grep ssh-agent$ > /dev/null || {
        start_agent;
    }
else
    start_agent;
fi

export NVM_DIR="/Users/kraske/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm

# Setting PATH for Python 3.7
# The original version is saved in .bash_profile.pysave
PATH="/Library/Frameworks/Python.framework/Versions/3.7/bin:${PATH}"
export PATH
